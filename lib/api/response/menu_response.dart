import 'dart:convert';

import 'package:flutterforfun/models/menu_item.dart';

MenuListResponse castResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return MenuListResponse.fromJson(jsonData);
}

class MenuListResponse {
  List<MenuItem> menu;

  MenuListResponse({this.menu});

  factory MenuListResponse.fromJson(Map<String, dynamic> json) {
    var list = json["data"] as List;
    List<MenuItem> parsedList = list.map((i) => MenuItem.fromJson(i)).toList();
    return MenuListResponse(menu: parsedList);
  }
}
