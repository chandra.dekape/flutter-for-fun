import 'package:flutterforfun/api/request/requests.dart';
import 'package:flutterforfun/api/response/menu_response.dart';
import 'package:http/http.dart' as api;
import 'package:flutterforfun/models/menu_item.dart';

Future<List<MenuItem>> getMenuList() async {
  final response = await api.get(menuList());
  print(response.request.url);
  return castResponseFromJson(response.body).menu;
}
