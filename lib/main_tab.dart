import 'package:flutter/material.dart';
import 'package:flutterforfun/tab_product.dart';

class TabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.white,
              child: TabBar(
                labelColor: Colors.purple,
                unselectedLabelColor: Colors.grey[700],
                tabs: [
                  Tab(text: "PRODUK"),
                  Tab(text: "RIWAYAT"),
                ],
                indicatorColor: Colors.orange,
              ),
            ),
            Expanded(
              child: TabBarView(
                children: <Widget>[
                  TabProduct(),
                  Container(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
