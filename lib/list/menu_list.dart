import 'package:flutter/material.dart';
import 'package:flutterforfun/models/menu_item.dart';

class MenuListItem extends StatelessWidget {
  MenuListItem(this.menu);

  final MenuItem menu;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.all(4),
      padding: EdgeInsets.all(8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.network(
            menu.categoryIcon,
            width: 48,
            height: 48,
          ),
          SizedBox(height: 4),
          Text(
            menu.categoryName,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 10,
            ),
          ),
        ],
      ),
    );
  }
}

class MenuList extends StatelessWidget {
  final List<MenuItem> items;

  MenuList(this.items);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(4),
      child: GridView.count(
        crossAxisCount: 4,
        childAspectRatio: 10 / 11,
        scrollDirection: Axis.vertical,
        shrinkWrap: false,
        children: List.generate(items.length, (index) {
          return MenuListItem(items[index]);
        }),
      ),
    );
  }
}
