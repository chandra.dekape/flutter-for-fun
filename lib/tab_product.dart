import 'package:flutter/material.dart';
import 'package:flutterforfun/api/service/menu_repository.dart' as Repository;
import 'package:flutterforfun/list/menu_list.dart';

class TabProduct extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TabProductState();
}

class TabProductState extends State<TabProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[100],
      child: FutureBuilder(
        future: Repository.getMenuList(),
        builder: (context, result) {
          if (result.hasError) {
            return Center(child: Text(result.error.toString()));
          } else if (result.hasData) {
            return MenuList(result.data);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
