import 'package:flutter/material.dart';

class HeaderMenu extends StatelessWidget {
  HeaderMenu({
    Key key,
    this.icon,
    this.onPressed
  }) : super(key: key);

  final String icon;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 42,
      height: 42,
      child: IconButton(
        icon: Image.asset(icon),
        onPressed: onPressed,
      ),
    );
  }
}
