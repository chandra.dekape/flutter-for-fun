import 'package:flutter/material.dart';
import 'package:flutterforfun/header_menu.dart';
import 'package:flutterforfun/main_tab.dart';
import 'constants/images.dart' as Images;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text("Warung BABE"),
          actions: <Widget>[
            Icon(
              Icons.notifications,
              color: Colors.white,
            )
          ],
        ),
        body: Column(
          children: <Widget>[
            Container(
              color: Colors.purple,
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                padding: EdgeInsets.all(16),
                child: Row(
                  children: <Widget>[
                    Icon(Icons.account_balance_wallet,
                        color: Colors.orange, size: 20),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      child: Text("Rp 100,000,000"),
                    ),
                    Icon(Icons.refresh, color: Colors.grey[800], size: 20),
                    Spacer(),
                    Icon(Icons.local_parking, color: Colors.orange, size: 20),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 8),
                      child: Text("1,000"),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              color: Colors.purple,
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  HeaderMenu(
                    icon: Images.IC_DEPOSIT,
                    onPressed: () {},
                  ),
                  HeaderMenu(
                    icon: Images.IC_TRANSFER,
                    onPressed: () {},
                  ),
                  HeaderMenu(
                    icon: Images.IC_MUTATION,
                    onPressed: () {},
                  ),
                  HeaderMenu(
                    icon: Images.IC_HELP,
                    onPressed: () {},
                  ),
                ],
              ),
            ),
            TabView()
          ],
        ),
      ),
    );
  }
}
